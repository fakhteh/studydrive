import Home from './pages/Home';
import ShowImages from './pages/ShowImages';
import Register from './pages/Register';
import Login from './pages/Login';

export default [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/register',
        component: Register,
        name: 'register'
    },
    {
        path: '/login',
        component: Login,
        name: 'login'
    },
    {
        path: '/show_images',
        component: ShowImages,
        name: 'show_images' 
    }
]