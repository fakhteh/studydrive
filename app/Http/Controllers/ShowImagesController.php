<?php

namespace App\Http\Controllers;

use App\Fav_Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShowImagesController extends Controller
{
    public function index() {
        return view('show_images');
    }

    public function all(Request $request) {
        $user_imgs = DB::table('fav_images')->where('user_id', $request->session()->get('user_id'))->get();
        return $user_imgs->toJson();
    }

    public function store(Request $request) {
    
        //$fav_image = new Fav_Image();
        $fav_image = $request->isMethod('put') ? Fav_Image::findOrFail($request->id) : new Fav_Image;

        $fav_image->fav_status = $request->input('fav_status');
        $fav_image->image_id = $request->input('image_id');
        $fav_image->id = $request->input('id');
        $fav_image->user_id = $request->session()->get('user_id');
        $post = $fav_image->save();
        return response()->json($post);

    }
}
