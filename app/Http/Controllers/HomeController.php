<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function get_users_most_fav()
    {
        
        $result  = DB::select("select user_id, sum(fav_status) as favs, name  
                                from fav_images tbl1 
                                inner join users tbl2 
                                on tbl1.user_id = tbl2.id 
                                where 1 
                                group by user_id 
                                order by favs DESC limit 5
                                ");
        return response()->json($result);

        /*$sql_imgs = DB::table('fav_images')
        ->join('users', 'users.id', '=', 'fav_images.user_id')
        ->select('user_id', DB::raw('sum(fav_status) as favs'))
        ->groupBy('user_id')
        ->orderBy('favs DESC')
        ->limit(5)
        ->get();

        return response()->json($sql_imgs);
        */
    }

    public function get_latest_fav_photos()
    {

        $sql_users = DB::table('users')
            ->select('id')
            ->get();


        $sql_imgs = DB::table('fav_images')
            ->select('image_id', DB::raw('group_concat(user_id) as fav_user_ids'))
            ->where('fav_status', '=', 1)
            ->groupBy('image_id')
            ->get();


        $users_fav_img = array();
        foreach ($sql_imgs as $sql_img) {
            $exist_flag = true;
            $user_fav_img = explode(',', $sql_img->fav_user_ids);

            foreach ($sql_users as $sql_user) {

                if (!in_array($sql_user->id, $user_fav_img)) {
                    $exist_flag = false;
                }
            }
            if ($exist_flag) {
                array_push($users_fav_img, $sql_img->image_id);
            }
        }

        return $users_fav_img;
    }
}
