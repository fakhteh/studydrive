<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('home_users_most_fav','HomeController@get_users_most_fav');
Route::get('home_latest_fav_photos','HomeController@get_latest_fav_photos');
Route::post('showImages','ShowImagesController@store');
Route::put('showImages','ShowImagesController@store');
Route::get('userFavs','ShowImagesController@all');